import { Component } from '@angular/core';
import { Sensor } from './shared/models/sensor.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sensors: Sensor[] = [];
  constructor() {
  for(var i=1; i < 11; i++) {
  this.sensors.push(new Sensor(i, `Датчик_${i}`));
  }
}

onDeleteSensor(idx: number) {
  this.sensors.splice(idx, 1);
}

onCreateSensor(new_name, new_status) {
  this.sensors.push(new Sensor(this.sensors[this.sensors.length - 1].id + 1, new_name.value, new_status.checked));
}
}
